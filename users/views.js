var Secure = imports('Secure');
const jwt = require('jsonwebtoken');
var models = imports('prebuild/userModel');
var HttpResponse = imports('response').HttpResponse;
var self = imports('users');
/*
	write apis here,
*/
// login function
exports.login = function(request, response){
	var user = request.body.name;
	var resp = {
		status : false,
		message : null,
		data : null
	};

	var code = 200;
	var u = new self.models.ChatUser({name : user});
	u.save().then(function(result){
		var signingObject = {
			name : result.name,
			_id : result._id
		}
		var token = jwt.sign(signingObject, 'RESTFULAPIs');
		var newUser = new self.models.UserToken({
			user : result._id,
			token : token
		})

		newUser.save().then(function(data){
			var responseData = {
				user : result,
				access_token : data.token
			}
			resp.status = true;
			resp.message = "Joined";
			resp.data = responseData;
			
			HttpResponse(response, resp, code);
		}).catch(function(err){
			resp.status = false;
			resp.message = "Unable to create token";
			resp.data = err;
			
			code  = 500;

			HttpResponse(response, resp, code);
		})
	}).catch(function(err){
		resp.status = false;
		resp.message = err;
		resp.data = null;
		code = 500;

		HttpResponse(response, resp, code)
	})
}