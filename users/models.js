var db = imports('db');

var self = imports('users');

var chatUserSchema = {
	name : {
		type : String,
		required : true
	},
	live : true,
	callback : self.socket.newUser,
	interval : 1000
}

var userToken = {
	user : {
		type : db.Schema.Types.ObjectId,
		ref : 'ChatUser'
	},
	token : {
		type : String,
		required : true
	}
}


// models
var chatUser = db.models('ChatUser', chatUserSchema);
var userToken = db.models('UserToken', userToken);

exports.ChatUser = chatUser;
exports.UserToken = userToken;