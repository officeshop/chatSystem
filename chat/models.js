var db = imports('db');
var self = imports('chat');

var messageSchema = {
	message : {
		type : String,
		required : true
	},
	user : {
		type : db.Schema.Types.ObjectId,
		ref : 'ChatUser'
	},
	likes : [
		{
			user : {
				type : db.Schema.Types.ObjectId,
				ref : 'ChatUser'
			}
		}
	],
	live : true,
	callback : self.socket.newMessage,
	interval : 1000
}

var message = db.models('Message', messageSchema);

exports.Message = message;