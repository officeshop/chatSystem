var Secure = imports('Secure');
var models = imports('prebuild/userModel');
var HttpResponse = imports('response').HttpResponse;
var self = imports('chat');
/*
	write apis here,
*/
// login function
exports.send = function(request, response){
	var message = request.body.message;
	var user_id = request.user._id;
	
	var resp = {
		status : false,
		message : null,
		data : null
	}

	var code = 200;

	var m = self.models.Message({
		message : message,
		user: user_id,
	})

	m.save().then(function(result){
		resp.status = true;
		resp.message = "Send";
		resp.data = result;

		HttpResponse(response,resp, code);
	}).catch(function(err){
		resp.message = err;

		code = 500;

		HttpResponse(response, resp, code);
	})
}

exports.like = function(request, response){
	var user_id = request.user._id;
	var message_id = request.body.message_id;

	var resp = {
		status : false,
		message : null,
		data : null
	}

	var code = 200;

	// get message
	self.models.Message.findOne({_id : message_id}).then(function(result){
		var likes = JSON.parse(JSON.stringify(result.likes));
		var obj = {
			user : user_id
		}

		var push = true;

		likes.map(function(v, k){
			if(v.user == user_id){
				push = false;
			}
		})

		if(push == true){
			likes.push(obj);
		
			self.models.Message.findOneAndUpdate({_id : message_id}, {likes : likes, time : new Date()}, {upsert: true, new:true, setDefaultsOnInsert: true}).then(function(newData){
				resp.status = true;
				resp.message = "liked";
				resp.data = newData;
				
				HttpResponse(response, resp, code)
			}).catch(function(err){
				resp.message = err;
				code = 500;
				HttpResponse(response, resp, code);
			})
		}
		else{
			resp.status = true;
			resp.message = "Cannot like twice";
			code = 200;

			HttpResponse(response, resp, code)
		}
	}).catch(function(err){
		resp.message = err;
		code = 500;

		HttpResponse(response, resp, code);
	})
}