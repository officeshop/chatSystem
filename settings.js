exports.root = __dirname+'/src';
exports.projectname = 'chatSystem';
exports.secret = 'LetsfindthewayoutthereV.1';

// database credentials
exports.db = { 
	database : 'chatSystem',
	host : '127.0.0.1',
	username : 'deep',
	password : 'deep',
	authSource : 'admin'
}

// fonts
exports.fonts = [
	'Roboto-Regular.ttf',
]

// INSTALLED_APPS
exports.INSTALLED_APPS=[
	"users",
	"chat"
]

// EXCLUDE_AUTH
exports.EXCLUDE_AUTH=[
	"/user/login/",
]

// Socket
exports.SOCKET_CONNECTION = true;

// keep alive socket
exports.KEEP_ALIVE_SOCKET = true;

// CORS
exports.ALLOW_CORS = true;

// CROSS ORIGIN
exports.ALLOW_ORIGIN = [
	"*"
]

// ALLOW HEADERS
exports.ALLOW_HEADERS = [
	"Origin",
	"X-Requested-With",
	"Content-Type",
	"Accept",
	"Authorization"
]

// ALLOW OPTIONS
exports.ALLOW_OPTIONS = [
	"GET",
	"POST",
	"OPTIONS",
	"PUT",
	"PATCH",
	"DELETE"
]