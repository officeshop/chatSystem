var path = require('path');
var webpack = require('webpack');
var BundleTracker = require('webpack-bundle-tracker');

module.exports = {
	context : __dirname,
	entry : './src/js/client.js',
	module : {
		loaders : [
			{
				test : /.(js|jsx)$/,
				exclude : /node_modules/,
				loader : 'babel-loader',
				query : {
					presets : ['react', 'es2015', 'stage-0'],
					plugins : ['react-html-attrs', 'transform-class-properties', 'transform-decorators-legacy']
				}
			},
		]
	},
	output : {
		path : path.resolve('./src/'),
		filename : 'client.min.js',
		// publicPath: 'http://localhost:8080/src/bundles/',
	},
	plugins : [
		new webpack.HotModuleReplacementPlugin(),
	],
};
