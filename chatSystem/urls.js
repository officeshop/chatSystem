
var Network = imports('Network');
var api = imports('api');
// methods
var method = api.method;

// apps
var users = imports('users');
var chat = imports('chat');

exports.api = function(router){
	method.page(router, '/', '/index.html');

	method.post(router, '/user/login', users.urls.login);
	method.post(router, '/chat/send', chat.urls.send);
	method.post(router, '/chat/like', chat.urls.like);
}

// connect your socket events
exports.sockets = function(){
	return {
		
	}
}
