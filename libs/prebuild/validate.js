// mandatory field
exports.empty = function(value){
	if(value.length !== 0) return true
	else return false
}

// email validation
exports.email = function(value){
	var format = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
	if(value.match(format)) return true;
	else return false;
}
