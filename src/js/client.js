import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, Link, hashHistory, IndexRoute } from 'react-router';
import {Provider} from 'react-redux';
import store from './store';
import { syncHistoryWithStore } from 'react-router-redux';

import Layout from './components/Layout';

const history = syncHistoryWithStore(hashHistory, store);

var app = document.getElementById('react-app');

ReactDOM.render(<Provider store = {store}>
	<div>
		<Router history = {history}>
			<Route path='/' component={Layout} />
		</Router>
	</div>
</Provider>, app);
