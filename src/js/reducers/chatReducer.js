export default function reducer(state = {
	user : null,
	sendMessage : null,
	messageLike : null
}, action){
	switch(action.type){
		case "LOGIN" : {
			return {...state, user : action.payload}
		}

		case "SEND_MESSAGE" : {
			return {...state, sendMessage : action.payload}
		}

		case "MESSAGE_LIKE" : {
			return {...state, messageLike : action.payload}
		}
	}

	return state;
}