import {combineReducers} from 'redux';
import { routerReducer } from 'react-router-redux';

import chat from './chatReducer';

export default combineReducers({
	chat,
	routing:routerReducer
})
