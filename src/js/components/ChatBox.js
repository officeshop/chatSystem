import React from 'react';
import { connect } from 'react-redux';

import openSocket from 'socket.io-client';

import {sendMessage, like} from './../actions/chatAction';

@connect((store) => {
	return {
		user : store.chat.user
	}
})
export default class ChatBox extends React.Component{
	constructor(props){
		super(props);
		this.state = {
			message : [],
			likes : 0
		}
	}

	componentDidMount(){
		const socket = openSocket('/', { query: "token=" + this.props.user.data.access_token});

		global.socket = socket;

		socket.on("newMessage", function(data){
			var newMes = Object.assign([], this.state.message);
			data.map(function(v, k){
				if(JSON.stringify(newMes).indexOf(v._id) !== -1){
					var replaceIndex = null;
					newMes.map(function(val, key){
						if(val._id == v._id){
							replaceIndex = key;
						}
					})
					newMes[replaceIndex] = v;
				}
				else{
					newMes.push(v);
				}
			})

			this.setState({
				message : newMes
			})
		}.bind(this))

		this.setState({
			socket : socket
		})
	}

	send(){
		var message = this.refs["message"].value;
		
		this.props.dispatch(sendMessage(message, this.props.user.data.access_token));
	
		this.refs["message"].value = "";
	}

	like(id){
		this.props.dispatch(like(id, this.props.user.data.access_token))
	}

	render(){
		return(
			<div className = 'container'>
				<div className = "wrapper-container">
					<center>
						<div className = "wrapper chatbox-wrapper">
							<div className = "chatbox">
								{
									this.state.message.map(function(value, index){
										if(value.user._id == this.props.user.data.user._id){
											return(
												<div key = {value._id} className = "me">
													<div className = "msg-block">
														{value.user.name+" : "+value.message}
													</div>
													<div className = "likes-block" onClick = {this.like.bind(this, value._id)}>
														<span style = {{textDecoration : "underline"}}>Likes</span> : {value.likes.length}
													</div>
												</div>
											)
										}
										else{
											return(
												<div key = {value._id} className = "they">
													<div className = "msg-block">
														{value.message+" : "+value.user.name}
													</div>
													<div className = "likes-block" onClick = {this.like.bind(this, value._id)}>
														<span style = {{textDecoration : "underline"}}>Likes</span> : {value.likes.length}
													</div>
												</div>
											)
										}
									}.bind(this))
								}
							</div>
							<div className = "send-input">
								<div className = "send-input-cell send-input-input">
									<input type = "text" placeholder = "Type your message" ref = "message" className = "send-input-field"/>
								</div>
								<div className = "send-input-cell">
									<div className = "send-input-button" onClick = {this.send.bind(this)}>
										<div className = "send-input-button-button">
											Send
										</div>
									</div>
								</div>
							</div>
						</div>
					</center>
				</div>
			</div>
		)
	}
}
