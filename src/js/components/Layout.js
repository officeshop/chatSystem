import React from 'react';
import {connect} from 'react-redux';
import ChatBox from './ChatBox';

import {login} from './../actions/chatAction';

@connect((store) => {
	return {
		user : store.chat.user
	}
})
export default class Layout extends React.Component{
	constructor(props){
		super(props);
		this.state = {
			loggedIn : false
		}
	}

	componentWillReceiveProps(nextProps){
		if(this.props.user !== nextProps.user){
			if(nextProps.user.status == true){
				this.setState({
					loggedIn : true
				})
			}
			else{
				alert("Unable to login");
			}
		}
	}

	submit(){
		var username = this.refs["username"].value;
		if(username.length !== 0){
			this.props.dispatch(login(username))
		}
	}

	render(){
		let body = (
			<div className = 'container'>
				<div className = "wrapper-container">
					<center>
						<div className = "wrapper">
							<div className = "heading">
								Enter Your Name
							</div>
							<div className = "input-name">
								<input ref = "username" type = "text" className = "input-name-input" placeholder = "Your Name"/>
							</div>
							<div className = "input-name">
								<button onClick = {this.submit.bind(this)} className = "login-button">
									Submit
								</button>
							</div>
						</div>
					</center>
				</div>
			</div>
		)

		if(this.state.loggedIn){
			body = <ChatBox />
		}

		return body;
	}
}
