import Conf from './../res/Conf';

export function login(username){
	return function(dispatch){
		var url = Conf.host+"user/login/";
		var xhttp = new XMLHttpRequest();
		xhttp.onreadystatechange = function(){
			if(xhttp.readyState == 4 && xhttp.status == 200){
				var resp = JSON.parse(xhttp.responseText);
				
				dispatch({
					type : "LOGIN",
					payload : resp
				})
			}
		}
		xhttp.open("POST", url, true);
		xhttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
		xhttp.send("name="+username);
	}
}

export function sendMessage(message, token){
	return function(dispatch){
		var url = Conf.host+"chat/send/";
		var xhttp = new XMLHttpRequest();
		xhttp.onreadystatechange = function(){
			if(xhttp.readyState == 4 && xhttp.status == 200){
				var resp = JSON.parse(xhttp.responseText);
				
				dispatch({
					type : "SEND_MESSAGE",
					payload : resp
				})
			}
		}
		xhttp.open("POST", url, true);
		xhttp.setRequestHeader("Authorization", "JWT "+token);
		xhttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
		xhttp.send("message="+message);
	}
}

export function like(id, token){
	return function(dispatch){
		var url = Conf.host+"chat/like/";
		var xhttp = new XMLHttpRequest();
		xhttp.onreadystatechange = function(){
			if(xhttp.readyState == 4 && xhttp.status == 200){
				var resp = JSON.parse(xhttp.responseText);
				
				dispatch({
					type : "MESSAGE_LIKE",
					payload : resp
				})
			}
		}
		xhttp.open("POST", url, true);
		xhttp.setRequestHeader("Authorization", "JWT "+token);
		xhttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
		xhttp.send("message_id="+id);
	}
}