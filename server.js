global.imports = require('./libs/imports.js');
global.importSocket = require('./libs/socket.js').import;
global.importModel = require('./libs/db.js').import;
const express = require('express');
const http = require('http');
const Auth = require('./libs/Auth.js');
const Cors = require('./libs/Cors.js');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const webpack = require('webpack')
const webpackDevMiddleWare = require('webpack-dev-middleware')
const webpackConfig = require('./webpack.config.js')
const api = express.Router();

// custom libs
const socket = imports('socket');
const settings = imports('settings');
const db = imports('db');
const transporter = imports('Transporter');
const route = require('./chatSystem/urls.js');

// initilizing express
const app = express();

// creating server
const server = http.createServer(app);

// configuring express 
app.use(express.static(settings.root+'/'));
app.use(cookieParser());
app.use(webpackDevMiddleWare(webpack(webpackConfig)))
app.use(bodyParser.urlencoded({ extended : true})); 
app.use(bodyParser.json());
app.use(Cors.allow);
app.use(Auth.token);
app.use('/', api);

// api routing 
route.api(api); 

// db connect 
db.connect(); 

// initilizing transport service
transporter.init();

// socket connection 
socket.connect(server);

// socket events 
route.sockets(); 

// starting server ] 
server.listen(3000); 
console.log('listening at http://localhost:8080/');